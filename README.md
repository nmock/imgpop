# ImgPop #

Retrieves a feed from Instagram and shows popular images from Flickr.

### Features ###

* Dynamic Cell Height (For Supported Services)
* Hiding Navigation Bar
* Pull To Refresh
* Pagination
* Video Playback (beta)

### Instructions / Notices ###
Build / run on device for full functionality / gestural support


### Improvements ###

* Network level & application level error handling (display to user)
* Track / cancel pending requests from other feeds on segment change
* Convey video status to user better
* Implement video placeholder delegate so doesn’t show artifacts of previous video
* Jumpy video badge positioning
* Cleanup / abstract some of the networking logic
* Explicit user authentication for services

### Instagram Test User ###

```
u: fullscreenmock p: fullsc33n
```

### Demo ###
[Demo Link](http://nathanmock.com/files/imgpop.m4v) (video cap from actual device)
![imgpop.m4v 2016-10-25 10-22-35.jpg](https://bitbucket.org/repo/Gk7G4o/images/3088514874-imgpop.m4v%202016-10-25%2010-22-35.jpg)