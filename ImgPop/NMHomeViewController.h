//
//  NMHomeViewController.h
//  ImgPop
//
//  Created by Nathan Mock on 10/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMHomeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@end

