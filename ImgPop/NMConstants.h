//
//  NMConstants.h
//  ImgPop
//
//  Created by Nathan Mock on 10/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#ifndef NMConstants_h
#define NMConstants_h

typedef void (^NMBlock)(void);
typedef void (^NMErrorBlock)(NSError *error);

#endif /* NMConstants_h */
