//
//  NMFeed.m
//  ImgPop
//
//  Created by Nathan Mock on 10/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "NMFeed.h"

@interface NMFeed ()

@end

@implementation NMFeed

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.reachedEnd = NO;
    }
    
    return self;
}

@end
