//
//  NMFlickrFeed.m
//  ImgPop
//
//  Created by Nathan Mock on 10/23/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "NMFlickrFeed.h"
#import <AFNetworking/AFHTTPSessionManager.h>

static NSString *const feed_description             = @"Flickr";

static NSString *const feed_url_string = @"https://api.flickr.com/services/rest/?method=flickr.interestingness.getList";

static NSString *const method_key_string            = @"method";
static NSString *const method_value_string          = @"flickr.interestingness.getList";

static NSString *const format_key_string            = @"format";
static NSString *const format_value_string          = @"json";

static NSString *const callback_key_string          = @"nojsoncallback";
static NSInteger const callback_value_number        = 1;

static NSString *const per_page_key_string          = @"per_page";
static NSInteger const per_page_value_number        = 5;

static NSString *const current_page_key_string      = @"page";
static NSString *const total_page_key_string        = @"total";

static NSString *const api_sig_key_string           = @"api_sig";
static NSString *const api_sig_value_string         = @"942ac8cc588d344d938ba7ebaa008a71";

static NSString *const access_token_key_string      = @"api_key";
static NSString *const access_token_value_string    = @"bbace9a4157f9dd888735d2e5ded7d3a";

static NSString *const photos_key_string            = @"photos";
static NSString *const photo_key_string             = @"photo";

@interface NMFlickrFeed ()
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger nextPage;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic, strong) NSDictionary *defaultParameters;
@end

@implementation NMFlickrFeed
- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.currentPage = self.nextPage = 1;
    }
    
    return self;
}

- (NSDictionary*)defaultParameters {
    return @{method_key_string              : method_value_string,
             access_token_key_string        : access_token_value_string,
             format_key_string              : format_value_string,
             callback_key_string            : @(callback_value_number),
             per_page_key_string            : @(per_page_value_number),
             current_page_key_string        : @(self.nextPage)};
}

- (void)getPosts:(NMBlock)success failure:(NMErrorBlock)failure {
    self.currentPage = self.nextPage = 1;
    self.reachedEnd = NO;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:feed_url_string parameters:self.defaultParameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //        NSLog(@"JSON: %@", responseObject);
        
        self.posts = @[];
        self.posts = [self mapPosts:responseObject];
        
        if (success) {
            success();
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        if (failure) {
            failure(error);
        }
    }];
}

- (void)getMorePosts:(NMBlock)success failure:(NMErrorBlock)failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:feed_url_string parameters:self.defaultParameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //        NSLog(@"JSON: %@", responseObject);

        self.posts = [self mapPosts:responseObject];
        
        if (success) {
            success();
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        if (failure) {
            failure(error);
        }
    }];
}

- (NSArray *)mapPosts:(NSDictionary*)responseObject {
    if (responseObject[photos_key_string]) {
        self.currentPage = [responseObject[photos_key_string][current_page_key_string] integerValue];
        self.nextPage = self.currentPage + 1;
        self.totalPages = [responseObject[photos_key_string][total_page_key_string] integerValue];
        
        if (self.currentPage < self.totalPages) {
            self.reachedEnd = NO;
        }
        else {
            self.reachedEnd = YES;
        }
    }
    
    if ([responseObject[photos_key_string][photo_key_string] isKindOfClass:[NSArray class]] && [((NSArray*)responseObject[photos_key_string][photo_key_string]) count] > 0) {
        NSMutableArray *posts = [NSMutableArray arrayWithArray:self.posts];
        
        for (NSDictionary *postDictionary in responseObject[photos_key_string][photo_key_string]) {
            NMFlickrPost *post = [[NMFlickrPost alloc] initWithDictionary:postDictionary];
            
            if (![self.posts containsObject:post]) {
                [posts addObject:post];
            }
        }
        
        return posts;
    }
    
    return nil;
}

- (NSString*)description {
    return feed_description;
}

@end
