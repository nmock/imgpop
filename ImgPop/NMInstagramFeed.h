//
//  NMInstagramFeed.h
//  ImgPop
//
//  Created by Nathan Mock on 10/23/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NMFeed.h"
#import "NMInstagramPost.h"

@interface NMInstagramFeed : NMFeed <NMFeedDelegate>
@end
