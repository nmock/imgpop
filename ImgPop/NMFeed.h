//
//  NMFeed.h
//  ImgPop
//
//  Created by Nathan Mock on 10/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NMConstants.h"

@interface NMFeed : NSObject
@property (nonatomic, strong) NSArray *posts;
@property (nonatomic, assign) BOOL reachedEnd;
@end

@protocol NMFeedDelegate
@required
- (void)getPosts:(NMBlock)success failure:(NMErrorBlock)failure;
- (void)getMorePosts:(NMBlock)success failure:(NMErrorBlock)failure;
@end
