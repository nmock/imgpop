//
//  NMImageView.h
//  ImgPop
//
//  Created by Nathan Mock on 10/24/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CTVideoPlayerView/CTVideoViewCommonHeader.h>

@interface NMImageView : UIImageView <CTVideoPlayerDownloadingViewProtocol>

@end
