//
//  NMFlickrPost.h
//  ImgPop
//
//  Created by Nathan Mock on 10/23/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "NMPost.h"

@interface NMFlickrPost : NMPost <NMPostDelegate>

@end
