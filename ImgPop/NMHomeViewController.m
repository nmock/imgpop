//
//  NMHomeViewController.m
//  ImgPop
//
//  Created by Nathan Mock on 10/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "NMHomeViewController.h"
#import <PureLayout/PureLayout.h>
#import <AMScrollingNavbar/AMScrollingNavbar.h>
#import "NMInstagramFeed.h"
#import "NMFlickrFeed.h"
#import "NMPostTableViewCell.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "UIView+ViewHelpers.h"

typedef enum {
    NMFeedSourceInstagram = 0,
    NMFeedSourceFlickr,
    NMFeedSourceCount
} NMFeedSource;

@interface NMHomeViewController ()
@property (nonatomic, assign) BOOL didSetupConstraints;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray<NMFeed<NMFeedDelegate> *> *feeds;
@property (nonatomic, strong) UIActivityIndicatorView *loadingIndicatorView;
@property (nonatomic, strong) UISegmentedControl *segmentedControl;
@property (nonatomic, weak) NMFeed<NMFeedDelegate> *currentFeed;
@end

@implementation NMHomeViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        self.tableView = [[UITableView alloc] initForAutoLayout];
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
        
        NMInstagramFeed *instagramFeed = [[NMInstagramFeed alloc] init];
        NMFlickrFeed *flickrFeed = [[NMFlickrFeed alloc] init];
        self.feeds = @[instagramFeed, flickrFeed];
        
        NSMutableArray *feedDescriptions = [NSMutableArray new];
        for (NMFeed *feed in self.feeds) {
            [feedDescriptions addObject:[feed description]];
        }
        
        self.segmentedControl = [[UISegmentedControl alloc] initWithItems:feedDescriptions];
        self.navigationItem.titleView = self.segmentedControl;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    [self.tableView setNeedsUpdateConstraints];
    
    [self.tableView registerClass:[NMPostTableViewCell class] forCellReuseIdentifier:@"NMPostTableViewCell"];
    
    typeof(self) weakSelf = self;
    
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf showLoading];
        [weakSelf.currentFeed getPosts:^{
            [weakSelf hideLoading];
            [weakSelf.tableView reloadData];
            [weakSelf.tableView.pullToRefreshView stopAnimating];
        } failure:^(NSError *error) {
            [weakSelf hideLoading];
            [weakSelf.tableView.pullToRefreshView stopAnimating];
        }];
    }];
    
    self.navigationItem.hidesBackButton = YES;
    
    [self.segmentedControl addTarget:self action:@selector(segmentedControlChanged:) forControlEvents:UIControlEventValueChanged];
    [self.segmentedControl setSelectedSegmentIndex:NMFeedSourceInstagram];
    [self segmentedControlChanged:self.segmentedControl];
}

- (void)updateViewConstraints {
    if (!self.didSetupConstraints) {
        [self.tableView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, 0, 0, 0) excludingEdge:ALEdgeTop];
        NSLayoutConstraint *topLayoutConstraint = [self.tableView autoPinToTopLayoutGuideOfViewController:self withInset:0];
        
        [self followScrollView:self.tableView usingTopConstraint:topLayoutConstraint withDelay:60.0];
        [self setShouldScrollWhenContentFits:NO];
        self.didSetupConstraints = YES;
    }
    
    [super updateViewConstraints];
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView {
    // This enables the user to scroll down the navbar by tapping the status bar.
    [self showNavbar];
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.currentFeed.posts count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NMPost *post = self.currentFeed.posts[indexPath.row];
    return [NMPostTableViewCell heightWithPost:post];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NMPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NMPostTableViewCell" forIndexPath:indexPath];
    
    NMPost *post = self.currentFeed.posts[indexPath.row];
    [cell updateWithPost:post];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // http://stackoverflow.com/a/25877725/349238
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row > [self.currentFeed.posts count] - 2 && !self.currentFeed.reachedEnd) {
        [self showLoading];
        [self.currentFeed getMorePosts:^{
            [self hideLoading];
            [self.tableView reloadData];
        } failure:^(NSError *error) {
            [self hideLoading];
        }];
    }
    
    if ([cell respondsToSelector:@selector(startVideo)]) {
        [cell performSelector:@selector(startVideo)];
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(stopVideo)]) {
        [cell performSelector:@selector(stopVideo)];
    }
}

- (NMFeed*)currentFeed {
    return self.feeds[self.segmentedControl.selectedSegmentIndex];
}

#pragma mark - UISegmentedControl State
- (void)segmentedControlChanged:(id)sender {
    self.tableView.contentOffset = CGPointZero;
    [self.tableView reloadData]; // show current feed data's cache
    [self.tableView triggerPullToRefresh];
}

#pragma mark - State
- (UIActivityIndicatorView*)loadingIndicatorView {
    if (_loadingIndicatorView) {
        return _loadingIndicatorView;
    }
    
    _loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _loadingIndicatorView.frame = CGRectMake(0, 0, 40.0, 40.0);
    _loadingIndicatorView.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1.00];
    [_loadingIndicatorView startAnimating];
    
    return _loadingIndicatorView;
}

- (void)showLoading {
    [self.loadingIndicatorView startAnimating];
    self.tableView.tableFooterView = self.loadingIndicatorView;
}

- (void)hideLoading {
    [self.loadingIndicatorView stopAnimating];
    self.tableView.tableFooterView = [UIView new];
}

- (void)showMessage:(NSString*)message title:(NSString*)title {
    if ([UIAlertController class]) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    else {
        
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        
    }
}

@end
