//
//  NMPostTableViewCell.m
//  ImgPop
//
//  Created by Nathan Mock on 10/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "NMPostTableViewCell.h"
#import <TTTAttributedLabel/TTTAttributedLabel.h>
#import "UIView+ViewHelpers.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <UIImage+ImageWithColor/UIImage+ImageWithColor.h>
#import "NMImageView.h"

@interface NMPostTableViewCell ()
@property (nonatomic, weak) NMPost *post;
@property (nonatomic, strong) NMImageView *previewImageView;
@property (nonatomic, strong) CTVideoView *videoView;
@property (nonatomic, strong) UIButton *videoButton;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@end

@implementation NMPostTableViewCell 

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [[UIScreen mainScreen] scale];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.previewImageView = [NMImageView new];
        self.previewImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.previewImageView.clipsToBounds = YES;
        [self addSubview:self.previewImageView];
        
        self.videoView = [CTVideoView new];
        self.videoView.contentMode = UIViewContentModeScaleAspectFill;
        self.videoView.clipsToBounds = YES;
        self.videoView.shouldPlayAfterPrepareFinished = YES;
        self.videoView.shouldReplayWhenFinish = YES;
        self.videoView.stalledStrategy = CTVideoViewStalledStrategyPlay;
        self.videoView.isSlideToChangeVolumeDisabled = YES;
        self.videoView.shouldShowCoverViewBeforePlay = YES;
        self.videoView.downloadingView = self.previewImageView;
        
        self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(togglePlayback)];
        [self.videoView addGestureRecognizer:self.tapGestureRecognizer];
        
        [self addSubview:self.videoView];
        
        self.videoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.videoButton.backgroundColor = [UIColor whiteColor];
        [self.videoButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.videoButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
        self.videoButton.layer.cornerRadius = 4.0;
        self.videoButton.alpha = 0.8;
        [self addSubview:self.videoButton];
    }
    return self;
}

- (void)updateWithPost:(NMPost*)post {
    self.post = post;
    
    UIColor *placeholderImageColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1.00];
    CGSize imageSize = CGSizeMake([UIView screenSize].width, ([UIView screenSize].width / post.imagePreviewWidth) * post.imagePreviewHeight);
    
    if (post.videoURL) {
        self.videoButton.hidden = NO;
        
        self.videoView.size = imageSize;
        self.videoView.videoUrl = post.videoURL; // mp4 playable
        [self.videoView play];
        
        [self.videoButton setTitle:[@" video " uppercaseString] forState:UIControlStateNormal];
        self.videoButton.top = kMargin;
        self.videoButton.centerX = self.centerX;
        [self.videoButton sizeToFit];
    }
    else {
        self.videoButton.hidden = YES;
        
        self.videoView.size = CGSizeZero;
        [self.videoView stopWithReleaseVideo:NO];
    }
    
    if (post.previewURL) {
        self.previewImageView.hidden = NO;
        
        CGPoint imageOrigin = CGPointZero;
    
        self.previewImageView.size = imageSize;
        self.previewImageView.origin = imageOrigin;
        [self.previewImageView setImageWithURL:post.previewURL placeholderImage:[UIImage imageWithColor:placeholderImageColor size:imageSize]];
    }
    else {
        self.previewImageView.size = CGSizeZero;
        self.previewImageView.image = nil;
    }
}

- (void)togglePlayback {
    if (self.post.videoURL) {
        [self.videoView play];
        self.videoView.isMuted = !self.videoView.isMuted;
    }
}

- (void)startVideo {
    if (self.post.videoURL) {
        [self.videoView play];
    }
}

- (void)stopVideo {
    if (self.post.videoURL) {
        [self.videoView stopWithReleaseVideo:NO];
    }
}

+ (CGFloat)heightWithPost:(NMPost*)post {
    CGFloat height;
    
    if (post.previewURL) {
        height += ([UIView screenSize].width / post.imagePreviewWidth) * post.imagePreviewHeight;
    }
    
    return height;
}

@end
