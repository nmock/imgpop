//
//  AppDelegate.h
//  ImgPop
//
//  Created by Nathan Mock on 10/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NMHomeViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) NMHomeViewController *homeViewController;

@end

