//
//  NMInstagramFeed.m
//  ImgPop
//
//  Created by Nathan Mock on 10/23/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "NMInstagramFeed.h"
#import <AFNetworking/AFHTTPSessionManager.h>

static NSString *const feed_description             = @"Instagram";

static NSString *const feed_url_string              = @"https://api.instagram.com/v1/users/self/media/recent/";

static NSString *const count_key_string             = @"count";
static NSInteger const per_page_value_number        = 3;

// ideally, would authenticate user, etc. but for the sake of brevity / simplicity, hard code access token
static NSString *const access_token_key_string      = @"access_token";
static NSString *const access_token_value_string    = @"4072027691.3148faf.f86d923b31d04c8c8add5c19904bc35e";

static NSString *const data_key_string              = @"data";

static NSString *const pagination_key_string        = @"pagination";
static NSString *const next_url_key_string          = @"next_url";

@interface NMInstagramFeed ()
@property (nonatomic, strong) NSDictionary *defaultParameters;
@property (nonatomic, strong) NSString *nextPageURLString;
@end

@implementation NMInstagramFeed

- (NSDictionary*)defaultParameters {
    return @{count_key_string               : @(per_page_value_number),
             access_token_key_string        : access_token_value_string};
}

- (void)getPosts:(NMBlock)success failure:(NMErrorBlock)failure {
    self.reachedEnd = NO;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:feed_url_string parameters:self.defaultParameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //        NSLog(@"JSON: %@", responseObject);
        
        self.posts = @[];
        self.posts = [self mapPosts:responseObject];
        
        if (success) {
            success();
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        if (failure) {
            failure(error);
        }
    }];
}


- (void)getMorePosts:(NMBlock)success failure:(NMErrorBlock)failure {
    if (self.reachedEnd || !self.nextPageURLString) {
        // error handling / display to user / call failure block
        NSLog(@"IG: Reached end of pagination.");
        return;
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:self.nextPageURLString parameters:self.defaultParameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //        NSLog(@"JSON: %@", responseObject);
        
        self.posts = [self mapPosts:responseObject];
        
        if (success) {
            success();
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        if (failure) {
            failure(error);
        }
    }];
}

- (NSArray *)mapPosts:(NSDictionary*)responseObject {
    if (responseObject[pagination_key_string][next_url_key_string]) {
        self.reachedEnd = NO;
        self.nextPageURLString = responseObject[pagination_key_string][next_url_key_string];
    }
    else {
        self.reachedEnd = YES;
        self.nextPageURLString = nil;
    }
    
    if ([responseObject[data_key_string] isKindOfClass:[NSArray class]] && [((NSArray*)responseObject[data_key_string]) count] > 0) {
        NSMutableArray *posts = [NSMutableArray arrayWithArray:self.posts];
        
        for (NSDictionary *postDictionary in responseObject[data_key_string]) {
            NMInstagramPost *post = [[NMInstagramPost alloc] initWithDictionary:postDictionary];
            
            if (![self.posts containsObject:post]) {
                [posts addObject:post];
            }
        }
        
        return posts;
    }
    
    return nil;
}

- (NSString*)description {
    return feed_description;
}

@end
