//
//  NMPost.m
//  ImgPop
//
//  Created by Nathan Mock on 10/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "NMPost.h"

@implementation NMPost

- (BOOL)isEqual:(id)object {
    if (![object isKindOfClass:[NMPost class]]) {
        return NO;
    }
    
    NMPost *post = object;
    return self == post || [self.identifier isEqualToString:post.identifier];
}
@end
