//
//  NMPostTableViewCell.h
//  ImgPop
//
//  Created by Nathan Mock on 10/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NMPost.h"
#import <CTVideoPlayerView/CTVideoViewCommonHeader.h>

@interface NMPostTableViewCell : UITableViewCell
- (void)updateWithPost:(NMPost*)post;
- (void)startVideo;
- (void)stopVideo;
+ (CGFloat)heightWithPost:(NMPost*)post;
@end
