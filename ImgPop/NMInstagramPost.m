//
//  NMInstagramPost.m
//  ImgPop
//
//  Created by Nathan Mock on 10/23/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "NMInstagramPost.h"

static NSString *const key_id                           = @"id";
static NSString *const key_user                         = @"user";
static NSString *const key_username                     = @"username";
static NSString *const key_caption                      = @"caption";
static NSString *const key_comments                     = @"comments";
static NSString *const key_count                        = @"count";
static NSString *const key_images                       = @"images";
static NSString *const key_standard_res                 = @"standard_resolution";
static NSString *const key_url                          = @"url";
static NSString *const key_width                        = @"width";
static NSString *const key_height                       = @"height";
static NSString *const key_type                         = @"type";
static NSString *const key_created_time                 = @"created_time";
static NSString *const key_videos                       = @"videos";
static NSString *const key_text                         = @"text";


static NSString *const value_image                      = @"image";
static NSString *const value_video                      = @"video";

/*
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *author;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) NSInteger numComments;
@property (nonatomic, strong) NSDate *createdDate;
@property (nonatomic, strong) NSURL *previewURL;
@property (nonatomic, strong) NSURL *videoURL;
@property (nonatomic, assign) NSInteger imagePreviewWidth;
@property (nonatomic, assign) NSInteger imagePreviewHeight;
@property (nonatomic, assign) BOOL isVideo;*/


@implementation NMInstagramPost
- (instancetype)initWithDictionary:(NSDictionary*)dictionary {
    self = [super init];
    
    if (self) {
        [self updateValuesFromDictionary:dictionary];
    }
    
    return self;
}

- (void)updateValuesFromDictionary:(NSDictionary*)dictionary {
    if (dictionary[key_id]) {
        self.identifier = dictionary[key_id];
    }
    
    if ([dictionary[key_user] isKindOfClass:[NSDictionary class]] && dictionary[key_user][key_username]) {
        self.author = dictionary[key_user][key_username];
    }
    
    if (dictionary[key_caption] && ![dictionary[key_caption] isEqual:[NSNull null]]) {
        self.title = dictionary[key_caption][key_text];
    }
    
    if ([dictionary[key_comments] isKindOfClass:[NSDictionary class]] && dictionary[key_comments][key_count]) {
        self.numComments = [dictionary[key_comments][key_count] integerValue];
    }
    
    if (dictionary[key_created_time]) {
        NSTimeInterval epochSeconds = [dictionary[key_created_time] doubleValue];
        self.createdDate = [NSDate dateWithTimeIntervalSince1970:epochSeconds];
    }
    
    if ([dictionary[key_images] isKindOfClass:[NSDictionary class]] && dictionary[key_images][key_standard_res]) {
        NSDictionary *standardRes = dictionary[key_images][key_standard_res];
        
        if (standardRes[key_url]) {
            self.previewURL = [NSURL URLWithString:standardRes[key_url]];
        }
        
        self.imagePreviewWidth = [standardRes[key_width] integerValue];
        self.imagePreviewHeight = [standardRes[key_height] integerValue];
    }
    
    if ([dictionary[key_videos] isKindOfClass:[NSDictionary class]] && dictionary[key_videos][key_standard_res]) {
        NSDictionary *standardRes = dictionary[key_videos][key_standard_res];
        
        if (standardRes[key_url]) {
            self.videoURL = [NSURL URLWithString:standardRes[key_url]];
        }
        
        self.isVideo = YES;
    }
}

- (NSString*)description {
    return @"";
}

@end
