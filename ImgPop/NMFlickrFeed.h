//
//  NMFlickrFeed.h
//  ImgPop
//
//  Created by Nathan Mock on 10/23/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "NMFeed.h"
#import "NMFlickrPost.h"

@interface NMFlickrFeed : NMFeed <NMFeedDelegate>
@end
