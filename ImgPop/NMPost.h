//
//  NMPost.h
//  ImgPop
//
//  Created by Nathan Mock on 10/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NMConstants.h"

@interface NMPost : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *author;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) NSInteger numComments;
@property (nonatomic, strong) NSDate *createdDate;
@property (nonatomic, strong) NSURL *previewURL;
@property (nonatomic, strong) NSURL *videoURL;
@property (nonatomic, assign) NSInteger imagePreviewWidth;
@property (nonatomic, assign) NSInteger imagePreviewHeight;
@property (nonatomic, assign) BOOL isVideo;
@end

@protocol NMPostDelegate
@required
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;
@end

