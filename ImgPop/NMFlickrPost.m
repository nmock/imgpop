//
//  NMFlickrPost.m
//  ImgPop
//
//  Created by Nathan Mock on 10/23/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "NMFlickrPost.h"
#import "UIView+ViewHelpers.h"

static NSString *const key_id                           = @"id";

// image resolving
static NSString *const key_farm_id                      = @"farm";
static NSString *const key_secret                       = @"secret";
static NSString *const key_server                       = @"server";

static NSString *const key_caption                      = @"title";


// ideally, would include extras, but the Flickr API is broken, not very fully featured

/*
 @property (nonatomic, strong) NSString *identifier;
 @property (nonatomic, strong) NSString *author;
 @property (nonatomic, strong) NSString *title;
 @property (nonatomic, assign) NSInteger numComments;
 @property (nonatomic, strong) NSDate *createdDate;
 @property (nonatomic, strong) NSURL *previewURL;
 @property (nonatomic, strong) NSURL *videoURL;
 @property (nonatomic, assign) NSInteger imagePreviewWidth;
 @property (nonatomic, assign) NSInteger imagePreviewHeight;
 @property (nonatomic, assign) BOOL isVideo;*/

// https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg


@implementation NMFlickrPost
- (instancetype)initWithDictionary:(NSDictionary*)dictionary {
    self = [super init];
    
    if (self) {
        [self updateValuesFromDictionary:dictionary];
    }
    
    return self;
}

- (void)updateValuesFromDictionary:(NSDictionary*)dictionary {
    if (dictionary[key_id]) {
        self.identifier = dictionary[key_id];
    }
    
    if (dictionary[key_caption]) {
        self.title = dictionary[key_caption];
    }
    
    if (self.identifier && dictionary[key_farm_id] && dictionary[key_secret] && dictionary[key_server]) {
        // https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
        // z = medium, 640 on longest side
        
        self.previewURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://farm%@.staticflickr.com/%@/%@_%@_z.jpg", dictionary[key_farm_id], dictionary[key_server], self.identifier, dictionary[key_secret]]];
    }
    
    self.imagePreviewWidth = [UIView screenSize].width;
    self.imagePreviewHeight = [UIView screenSize].width;
}

- (NSString*)description {
    return @"";
}

@end
